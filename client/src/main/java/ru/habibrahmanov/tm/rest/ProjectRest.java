package ru.habibrahmanov.tm.rest;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.RedirectView;
import ru.habibrahmanov.tm.dto.ProjectDto;
import ru.habibrahmanov.tm.dto.ProjectDtos;
import ru.habibrahmanov.tm.enumeration.Status;

import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Named
@SessionScoped
public class ProjectRest {

    @Nullable private String id;
    @Nullable private String name;
    @Nullable private String description;
    @Nullable private String status;
    @Nullable private String dateBegin;
    @Nullable private String dateEnd;

    @NotNull final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @NotNull final static String URL = "http://localhost:8080/";

    public RedirectView create(@NotNull final String name, @NotNull final String description, @NotNull final String dateBegin,
                               @NotNull final String dateEnd) throws ParseException {
        RestTemplate restTemplate = new RestTemplate();
        ProjectDto projectDto = new ProjectDto();
        projectDto.setName(name);
        projectDto.setDescription(description);
        projectDto.setDateBegin(dateFormat.parse(dateBegin));
        projectDto.setDateEnd(dateFormat.parse(dateEnd));
        restTemplate.postForObject(URL + "project-create", projectDto, ProjectDto.class);
        return new RedirectView("project");
    }

    public RedirectView update(
            @NotNull final String id, @NotNull final String name, @NotNull final String description, @NotNull final Status status,
            @NotNull final String dateBegin, @NotNull final String dateEnd
    ) throws ParseException {
        RestTemplate restTemplate = new RestTemplate();
        ProjectDto projectDto = new ProjectDto();
        projectDto.setId(id);
        projectDto.setName(name);
        projectDto.setDescription(description);
        projectDto.setStatus(status);
        projectDto.setDateBegin(dateFormat.parse(dateBegin));
        projectDto.setDateEnd(dateFormat.parse(dateEnd));
        System.out.println("name " + projectDto.getName());
        restTemplate.put(URL + "project-update", projectDto, ProjectDto.class);
        return new RedirectView("project");
    }

    public RedirectView removeOne(@NotNull final String projectId){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL + "project-delete/" + projectId);
        return new RedirectView("project");
    }

    public ProjectDto findOne(@NotNull final String projectId){
        RestTemplate restTemplate = new RestTemplate();
        ProjectDto projectDto = restTemplate.getForObject(URL + "/projects/" + projectId, ProjectDto.class);
        return projectDto;
    }

    public List<ProjectDto> getFindAll() {
        RestTemplate restTemplate = new RestTemplate();
        ProjectDtos projectDtos = restTemplate.getForObject(URL + "all-projects", ProjectDtos.class);
        return projectDtos.getProjectDtos();
    }
}