package ru.habibrahmanov.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.enumeration.Status;
import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "app_task")
@NoArgsConstructor
public final class Task {

    @Id
    @Column(name = "id")
    protected String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Column(name = "date_begin")
    private Date dateBegin;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Column(name = "date_end")
    private Date dateEnd;

    @Enumerated(value = EnumType.STRING)
    private Status status = Status.PLANNED;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

    public Task(@NotNull final String name, @NotNull final String description, @NotNull final Date dateBegin,
                @NotNull final Date dateEnd, @NotNull final Project project) {
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        this.project = project;
    }
}
