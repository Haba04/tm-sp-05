package ru.habibrahmanov.tm.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import ru.habibrahmanov.tm.dto.ProjectDto;
import ru.habibrahmanov.tm.dto.ProjectDtos;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.api.IProjectService;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ProjectRestController {

    @Autowired
    private IProjectService projectService;

    @Nullable
    @GetMapping("/projects")
    public List<ProjectDto> findAll() {
        @Nullable final List<Project> projectList = projectService.findAll();
        @Nullable final List<ProjectDto> projectDtoList = new ArrayList<>();
        for (Project project : projectList) {
            projectDtoList.add(convertEntityToDto(project));
        }
        return projectDtoList;
    }

    @Nullable
    @GetMapping("/all-projects")
    public ProjectDtos getAllProjects(){
        @Nullable final List<Project> projectList = projectService.findAll();
        @Nullable final List<ProjectDto> projectDtoList = new ArrayList<>();
        for (Project project : projectList) {
            projectDtoList.add(convertEntityToDto(project));
        }
        ProjectDtos projectDtos = new ProjectDtos();
        projectDtos.setProjectDtos(projectDtoList);
        return projectDtos;
    }

    @Nullable
    @GetMapping("/projects/{projectId}")
    public ProjectDto findOne(@NotNull @PathVariable(name = "projectId") final String projectId){
        System.out.println("********* projectID " + projectId);
        return convertEntityToDto(projectService.findOne(projectId));
    }

    @DeleteMapping("/project-delete/{projectId}")
    public RedirectView delete(@NotNull @PathVariable(name = "projectId") final String projectId){
        projectService.removeOne(projectId);
        return new RedirectView("/projects");
    }

    @GetMapping("/project-create")
    public RedirectView create(
            @NotNull @RequestParam(name = "name") final String name, @Nullable @RequestParam(name = "description") final String description,
            @Nullable @RequestParam(name = "dateBegin") final String dateBegin, @RequestParam(name = "dateEnd") @Nullable final String dateEnd
    ) throws ParseException {
        projectService.insert(name, description, dateBegin, dateEnd);
        return new RedirectView("/projects");
    }

    @PostMapping("/project-create")
    public RedirectView insert(@Nullable @RequestBody final ProjectDto projectDto) throws ParseException {
        projectService.persist(convertDtoToEntity(projectDto));
        return new RedirectView("/projects");
    }

    @PutMapping("/project-update")
    public RedirectView update(
            @Nullable @RequestBody final Project project
    ) throws ParseException {
        System.out.println("name " + project.getName());
        projectService.update(project.getId(), project.getName(), project.getDescription(), project.getStatus(), project.getDateBegin(), project.getDateEnd());
        return new RedirectView("/projects");
    }

    @GetMapping("/project-update")
    public RedirectView update(
            @NotNull @RequestParam(name = "id") final String projectId, @NotNull @RequestParam(name = "name") final String name, @Nullable @RequestParam(name = "description", required = false) final String description,
            @Nullable @RequestParam(name = "status") final String status, @Nullable @RequestParam(name = "dateBegin") final String dateBegin, @RequestParam(name = "dateEnd") @Nullable final String dateEnd
    ) throws ParseException {
        projectService.update(projectId, name, description, status, dateBegin, dateEnd);
        return new RedirectView("/projects");
    }

    @Nullable
    public ProjectDto convertEntityToDto(@Nullable final Project project) {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescription());
        projectDto.setStatus(project.getStatus());
        projectDto.setDateBegin(project.getDateBegin());
        projectDto.setDateEnd(project.getDateEnd());
        return projectDto;
    }

    @Nullable
    public Project convertDtoToEntity(@Nullable final ProjectDto projectDto) {
        Project project = new Project();
        project.setId(projectDto.getId());
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setStatus(projectDto.getStatus());
        project.setDateBegin(projectDto.getDateBegin());
        project.setDateEnd(projectDto.getDateEnd());
        return project;
    }
}
