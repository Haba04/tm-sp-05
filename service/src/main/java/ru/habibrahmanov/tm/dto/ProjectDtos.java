package ru.habibrahmanov.tm.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ProjectDtos {
    private List<ProjectDto> projectDtos;
}
