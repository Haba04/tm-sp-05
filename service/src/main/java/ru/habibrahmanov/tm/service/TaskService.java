package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.habibrahmanov.tm.api.ITaskService;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.enumeration.Status;
import ru.habibrahmanov.tm.repository.ITaskRepository;
import java.text.ParseException;
import java.util.List;

@Service
public class TaskService extends AbstractService implements ITaskService {

    @Autowired
    private ITaskRepository taskRepository;

    @Override
    public void persist(@NotNull final Task task) {
        taskRepository.save(task);
    }

    @Override
    public void insert(
            @NotNull final Project project, @NotNull final String name,
            @NotNull final String description, @NotNull final String dateBegin, @NotNull final String dateEnd
    ) throws ParseException {
        taskRepository.save(new Task(name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd), project));
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @Nullable final Task task = taskRepository.findById(id).orElse(null);
        return task;
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String projectId, @Nullable final String id) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @Nullable final Task task = taskRepository.findByProjectIdAndId(projectId, id);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        @NotNull final List<Task> taskList = taskRepository.findByProjectId(projectId);
        return taskList;
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @NotNull final List<Task> taskList = taskRepository.findAll();
        return taskList;
    }

    @Transactional
    @Override
    public void removeOne(@Nullable final String projectId, @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        taskRepository.deleteByProjectIdAndId(projectId, taskId);
    }

    @Transactional
    @Override
    public void removeAll(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.deleteAllByProjectId(projectId);
    }

    @Transactional
    @Override
    public void update(@NotNull final Task task
    ) throws ParseException {
        taskRepository.update(task.getProject().getId(), task.getId(), task.getName(), task.getDescription(), task.getStatus(), task.getDateBegin(), task.getDateEnd());
    }

    @Transactional
    @Override
    public void update(@NotNull final String projectId, @NotNull final String id, @NotNull final String name,
                       @NotNull final String description, @NotNull final Status status, @NotNull final String dateBegin, @NotNull final String dateEnd
    ) throws ParseException {
        taskRepository.update(projectId, id, name, description, status, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
    }

    @NotNull
    @Override
    public List<Task> searchByString(
            @NotNull final String taskId, @NotNull final String string
    ) {
        @NotNull final List<Task> taskList = taskRepository.searchByString(taskId, string);
        return taskList;
    }
}
